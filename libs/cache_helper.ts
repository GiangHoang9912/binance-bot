import { createClient } from "redis";
import { getConfig } from "./get_config";

export const redisCacheClient = createClient({ url: getConfig("REDIS_URI") });

export const connectRedis = async () => {
  await redisCacheClient
    .connect()
    .catch((err) => console.log(`Redis connect error: ${err}`));
};
const label = "Redis cache connect";

redisCacheClient.on("connect", () => {
  console.log(label);
});

export const cacheHelper = {
  setKeyValueExpire(
    key: string,
    data: any,
    expireInSeconds = 7 * 24 * 60 * 60
  ): any {
    void redisCacheClient.setEx(`c:${key}`, expireInSeconds, data);
  },
  async getKey(key: string) {
    return await redisCacheClient.get(`${key}`);
  },
  async getKeyRegex(key: string) {
    return await redisCacheClient.keys(`c:${key}`);
  },
  async delKey(key: string) {
    return await redisCacheClient.del(`${key}`);
  },
  async getKeyIfNotDoSet(
    key: string,
    handlerReturnData: () => Promise<any>,
    expireInSeconds = 7 * 24 * 60 * 60
  ): Promise<any> {
    let data = await cacheHelper.getKey(`c:${key}`);
    if (!data) {
      data = await handlerReturnData();
      await cacheHelper.setKeyValueExpire(`c:${key}`, data, expireInSeconds);
    }
    return data;
  },
  async clearAllCache() {
    return await redisCacheClient.flushAll();
  },
};
