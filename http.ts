import Koa from "koa";
import mount from "koa-mount";

import {
  httpLoggerMiddleware,
  middlewareBodyParser,
  middlewareCors,
  middlewareHandle404,
  middlewareHandleError,
  middlewareHealthCheck,
  middlewareQueryString,
  middlewareSetResponseHeader,
} from "./libs/middlewares/general";

export const httpServer = new Koa();
middlewareQueryString(httpServer);
httpServer.use(middlewareCors());
httpServer.use(middlewareSetResponseHeader());
httpServer.use(httpLoggerMiddleware());
httpServer.use(middlewareHandleError());
httpServer.use(middlewareBodyParser());

httpServer.use(mount("/api/health", middlewareHealthCheck()));

httpServer.use(middlewareHandle404());
