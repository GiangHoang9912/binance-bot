export namespace BinanceType {
  type BinanceTick = {
    time: Date,
    open: number,
    high: number,
    low: number,
    close: number,
    volume: number,
    closeTime: Date,
    assetVolume: number,
    trades: number,
    buyBaseVolume: number,
    buyAssetVolume: number,
    ignored: number,
  };
  type MiniTicket = {
    eventType: string,
    eventTime: 1663569663558,
    symbol: string,
    close: string,
    open: string,
    high: string,
    low: string,
    volume: string,
    quoteVolume: string
  }
}
