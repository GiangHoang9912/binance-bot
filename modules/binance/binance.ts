import { helper } from './../../libs/helper';
import { TelegramBotHelper } from './../telebot/helper';
import { MyError } from './../../libs/errors';
import { Constants, periods } from '../../constants';
import { BinanceType } from './type';
import { TeleBot } from '../../modules/telebot/type';
import * as fs from "fs"

const Binance = require("node-binance-api");

const binance = new Binance().options({
  APIKEY: Constants.BINANCE.APIKEY,
  APISECRET: Constants.BINANCE.APISECRET,
});

export const FuturePrice = async (): Promise<string> => {
  return await binance.futuresPrices();
};

export const LimitBuy = async (): Promise<void> => {
  await binance.futuresBuy("BTCUSDT", 0.1, 8222);
};

export const LimitSell = async (): Promise<void> => {
  await binance.futuresSell("BTCUSDT", 0.5, 11111);
};

export const MarketBuy = async (): Promise<void> => {
  await binance.futuresMarketBuy("BNBUSDT", 5);
};

export const MarketSell = async (): Promise<void> => {
  await binance.futuresMarketSell("TRXUSDT", 1);
};

export const FutureChart = async (symbol: string, periods: periods): Promise<void> => {
  binance.futuresChart(symbol, periods, console.log);
};

export const MiniTicker = async (symbol: string): Promise<BinanceType.MiniTicket> => {
  return new Promise((resolve, reject) => {
    binance.futuresMiniTickerStream('BTCUSDT', (tick: any) => {
      resolve(tick);
    });
  });
};

export const PlaceMutipleOrder = async (): Promise<void> => {
  let orders = [
    {
      symbol: "BNBUSDT",
      side: "SELL",
      type: "MARKET",
      quantity: "0.05",
    },
  ];
  console.info(await binance.futuresMultipleOrders(orders));
};

export const CandlestickData = async (symbol: string, period: periods): Promise<BinanceType.BinanceTick[]> => {
  return new Promise(async (res: any, rej: any) => {
    const startTime = Date.now() - 1000 * 60 * 60 * 24;
    console.log("startTime", new Date(startTime));
    const endTime = Date.now()
    console.log("endTime", new Date(endTime));
    await binance.candlesticks(
      symbol.toUpperCase(),
      period,
      (error: any, ticks: any[], symbol: string) => {
        const data = ticks.map((tick: any) => {
          const [time, open, high, low, close, volume, closeTime, assetVolume, trades, buyBaseVolume, buyAssetVolume, ignored] = tick;
          const tickData: BinanceType.BinanceTick = {
            time: new Date(time),
            open: Number(open),
            high: Number(high),
            low: Number(low),
            close: Number(close),
            volume: Number(volume),
            closeTime: new Date(closeTime),
            assetVolume: Number(assetVolume),
            trades: parseInt(trades),
            buyBaseVolume: Number(buyBaseVolume),
            buyAssetVolume: Number(buyAssetVolume),
            ignored: Number(ignored),
          };

          return tickData;
        });

        return res(data);
      },
      { limit: 500, startTime: startTime, endTime: endTime }
    );
  })
};

export const CandlestickWebSocket = async (bot: any, symbol: string, period: periods, botType: TeleBot.BotType): Promise<void> => {
  // Periods: 1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
  await TelegramBotHelper.initMaxMin(symbol, period);
  setInterval(() => {
    const munite = new Date().getMinutes()
    const second = new Date().getSeconds()
    console.log(munite)
    if (bot && (munite === 15 || munite === 30 || munite === 47 || munite === 0) && second === 0) {
      const rawdata = fs.readFileSync('./last-signal.json', 'utf8');
      const data = JSON.parse(rawdata)
      if (data && data.text) {
        bot?.sendMessage(botType.chatId, data.text);
      }
    }
  }, 900);
  binance.websockets.candlesticks([symbol], period, async (candlesticks: any) => {
    let { e: eventType, E: eventTime, s: currentSymbol, k: ticks } = candlesticks;
    let {
      o: open,
      h: high,
      l: low,
      c: close,
      v: volume,
      n: trades,
      i: interval,
      x: isFinal,
      q: quoteVolume,
      V: buyVolume,
      Q: quoteBuyVolume,
    } = ticks;

    const text = await TelegramBotHelper.getDataBuySymbol(symbol, period, Number(low), Number(high), Number(close));

    if (text) {
      await helper.wait(5000);

      bot.sendMessage(botType.chatId, text);
    }
  });
};

export const LastPriceWebSocket = async (symbol: string, period: periods): Promise<void> => {
  binance.websockets.chart("BTCUSDT", "15m", (symbol: string, interval: string, chart: any) => {
    let tick = binance.last(chart);
    const last = chart[tick].close;
    console.info(chart);
    // Optionally convert 'chart' object to array:
    // let ohlc = binance.ohlc(chart);
    // console.info(symbol, ohlc);
    console.info(symbol + " last price: " + last)
  });
}
