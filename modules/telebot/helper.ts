import { helper } from './../../libs/helper';
import { FutureChart, MiniTicker } from './../binance/binance';
import { BinanceType } from "../../modules/binance/type";
import { periods, Trend } from "../../constants";
import { Config } from "../../constants";
import { CandlestickData } from "../../modules/binance/binance";
import * as fs from "fs"

enum TradeType {
  LONG = "LONG",
  SHORT = "SHORT",
  NONE = "NONE",
};

export let lastSignal = "";

function convertTime(date: Date) {
  return new Date(date).toLocaleString("en", {
    timeZone: "Asia/Ho_Chi_Minh",
  })
}
// https://fapi.binance.com/fapi/v1/markPriceKlines?symbol=BTCUSDT&interval=15m
async function getCandles() {
  let data = await helper.makeRequest({ url: "https://www.binance.com/fapi/v1/continuousKlines?limit=7&pair=BTCUSDT&contractType=PERPETUAL&interval=15m" })
  data = JSON.parse(data)
  return data.map((tick: any) => {
    const [time, open, high, low, close, volume, closeTime, assetVolume, trades, buyBaseVolume, buyAssetVolume, ignored] = tick;
    const tickData: BinanceType.BinanceTick = {
      time: new Date(time),
      open: Number(open),
      high: Number(high),
      low: Number(low),
      close: Number(close),
      volume: Number(volume),
      closeTime: new Date(closeTime),
      assetVolume: Number(assetVolume),
      trades: parseInt(trades),
      buyBaseVolume: Number(buyBaseVolume),
      buyAssetVolume: Number(buyAssetVolume),
      ignored: Number(ignored),
    };
    return tickData
  })
}

export const TelegramBotHelper = {
  status: Trend.NONE,
  lastTrend: Trend.NONE,
  lastStatus: Trend.NONE,
  min: Number,
  max: Number,
  lastMin: Number,
  lastMax: Number,
  message(symbol: string) {
    if ((this.status === Trend.UP || this.status === Trend.DOWN) && this.status !== this.lastTrend) {
      const formatNumber = 2;
      const entry1 = this.status === Trend.UP ? this.min * ((100 + Config.ENTRY.ENTRY1) / 100) : this.max * ((100 - Config.ENTRY.ENTRY1) / 100)
      const entry2 = this.status === Trend.UP ? this.min * ((100 + Config.ENTRY.ENTRY2) / 100) : this.max * ((100 - Config.ENTRY.ENTRY2) / 100)
      const entry3 = this.status === Trend.UP ? this.min * ((100 + Config.ENTRY.ENTRY3) / 100) : this.max * ((100 - Config.ENTRY.ENTRY3) / 100)
      const sl = this.status === Trend.UP ? this.min * ((100 - Config.SL.SL_RATIO) / 100) : this.max * ((100 + Config.SL.SL_RATIO) / 100)
      const tp1 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP1) / 100) : this.lastMin * ((100 + Config.TP.TP1) / 100)
      const tp2 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP2) / 100) : this.lastMin * ((100 + Config.TP.TP2) / 100)
      const tp3 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP3) / 100) : this.lastMin * ((100 + Config.TP.TP3) / 100)
      const tp4 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP4) / 100) : this.lastMin * ((100 + Config.TP.TP4) / 100)
      const tp5 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP5) / 100) : this.lastMin * ((100 + Config.TP.TP5) / 100)
      const tp6 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP6) / 100) : this.lastMin * ((100 + Config.TP.TP6) / 100)
      const tp7 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP7) / 100) : this.lastMin * ((100 + Config.TP.TP7) / 100)
      const tp8 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP8) / 100) : this.lastMin * ((100 + Config.TP.TP8) / 100)
      const tp9 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP9) / 100) : this.lastMin * ((100 + Config.TP.TP9) / 100)
      const tp10 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP10) / 100) : this.lastMin * ((100 + Config.TP.TP10) / 100)
      const tp11 = this.status === Trend.DOWN ? this.max * ((100 - Config.TP.TP11) / 100) : this.lastMin * ((100 + Config.TP.TP11) / 100)
      console.log("send")
      let message = `Symbol: ${symbol}`
      //  lệnh
      message += this.status === Trend.UP ? `\nTrade: ${TradeType.LONG}` : `\nTrade: ${TradeType.SHORT}`
      //  đáy đỉnh
      message += this.status === Trend.UP ? `\nĐáy: ${this.min} \nĐỉnh: ${this.lastMax}` : `\nĐỉnh: ${this.max} \nĐáy: ${this.lastMin}`
      message += "\nEntry:"
      // entry 1
      message += `\n-Entry1: ${entry1.toFixed(formatNumber)}`
      // entry 2
      message += `\n-Entry2: ${entry2.toFixed(formatNumber)}`
      // entry 3
      message += `\n-Entry3: ${entry3.toFixed(formatNumber)}`
      // SL
      message += `\nSL: \n-SL: ${sl.toFixed(formatNumber)}`
      // TP
      message += "\nTP: "
      // Tp
      message += `\n-TP1: ${tp1.toFixed(formatNumber)}`
      message += `\n-TP2: ${tp2.toFixed(formatNumber)}`
      message += `\n-TP3: ${tp3.toFixed(formatNumber)}`
      message += `\n-TP4: ${tp4.toFixed(formatNumber)}`
      message += `\n-TP5: ${tp5.toFixed(formatNumber)}`
      message += `\n-TP6: ${tp6.toFixed(formatNumber)}`
      message += `\n-TP7: ${tp7.toFixed(formatNumber)}`
      message += `\n-TP8: ${tp8.toFixed(formatNumber)}`
      message += `\n-TP9: ${tp9.toFixed(formatNumber)}`
      message += `\n-TP10: ${tp10.toFixed(formatNumber)}`
      message += `\n-TP11: ${tp11.toFixed(formatNumber)}`
      console.log(message)

      fs.writeFile('last-signal.json', JSON.stringify({
        symbol: symbol,
        trade: this.status === Trend.UP ? TradeType.LONG : TradeType.SHORT,
        entry1: entry1.toFixed(formatNumber),
        entry2: entry2.toFixed(formatNumber),
        entry3: entry3.toFixed(formatNumber),
        min: this.status === Trend.UP ? this.min : this.lastMin,
        max: this.status === Trend.UP ? this.lastMax : this.max,
        sl: sl.toFixed(formatNumber),
        tp1: tp1.toFixed(formatNumber),
        tp2: tp2.toFixed(formatNumber),
        tp3: tp3.toFixed(formatNumber),
        tp4: tp4.toFixed(formatNumber),
        tp5: tp5.toFixed(formatNumber),
        tp6: tp6.toFixed(formatNumber),
        tp7: tp7.toFixed(formatNumber),
        tp8: tp8.toFixed(formatNumber),
        tp9: tp9.toFixed(formatNumber),
        tp10: tp10.toFixed(formatNumber),
        tp11: tp11.toFixed(formatNumber),
        text: message
      }, null, 2), (err) => {
        if (err) throw err;
        console.log('Data written to file');
      });

      lastSignal = message

      this.lastMin = this.min
      this.lastMax = this.max

      return message
    }
  },
  async initMaxMin(symbol: string, period: periods) {
    // const tick = await MiniTicker(symbol)
    this.lastMax = 19140
    this.lastMin = 18460
    this.max = 19140
    this.min = 18460
    this.lastTrend = Trend.UP
    this.status = Trend.DOWN
    // let index = 375
    // setInterval(async () => {
    //     let rawdata = fs.readFileSync('./data.json', 'utf8');
    //     arrayTick = (JSON.parse(rawdata) as BinanceType.BinanceTick[])
    //     index++
    //     arrayTick = arrayTick.slice(0, index)
    // }, 4000)
  },
  async getDataBuySymbol(symbol: string = "BTCUSDT", period: periods = periods.M15, low: number, high: number, current: number): Promise<any> {

    try {
      // if (this.lastTrend !== this.status) {
      // this.min = low;
      // this.max = high;
      const compareCandle = 5
      // const array5CandleNear = [...arrayTick].slice(-compareCandle)

      //@todo: lấy giá nên thời gian thực
      const array5CandleNear = await getCandles()
      for (const candle of array5CandleNear) {
        console.log(`time 5 cây: ${convertTime(candle.time)} - max: ${candle.high} - min: ${candle.low}`)
      }
      // arrayTick.reverse()
      //@todo: ép công thức để đổi trend
      const currentTick = array5CandleNear[array5CandleNear.length - 2]
      console.log("time hiện tại: ", convertTime(currentTick.time))
      console.log(`đỉnh cũ ${this.lastMax} - đáy cũ ${this.lastMin} - đỉnh hiện tại ${this.max} - đáy hiện tại ${this.min} - trend ${this.status} - last trend ${this.lastTrend}`)


      if (currentTick.high > this.max) {
        this.max = currentTick.high
        this.status = Trend.UP
        this.lastTrend = Trend.DOWN
      }
      if (currentTick.low < this.min) {
        this.min = currentTick.low
        this.status = Trend.DOWN
        this.lastTrend = Trend.UP
      }

      if (this.lastTrend === Trend.UP && this.status === Trend.DOWN) {
        let minOf5Candle = array5CandleNear[2]
        let checkMin = false;
        for (let i = 1; i < compareCandle + 2; i++) {
          if ((currentTick.close / this.lastMin - 1) * 100 >= Config.REBOUND_FACTOR) {
            console.log(`time đạt đáy: ${convertTime(minOf5Candle.time)} - ${minOf5Candle.high} - ${minOf5Candle.low}`)
            console.log(`đỉnh cũ ${this.lastMax} - đáy cũ ${this.lastMin} - đỉnh hiện tại ${this.max} - đáy hiện tại ${this.min} - trend ${this.status} - last trend ${this.lastTrend}`)
            if (array5CandleNear[i].close < this.min) {
              minOf5Candle = array5CandleNear[i]
            }
            if (this.lastTrend === Trend.UP && this.status === Trend.DOWN) {
              this.lastTrend = Trend.DOWN
              this.status = Trend.UP
              checkMin = true
            }
          }
        }
        if (checkMin) {
          this.message(symbol)
          this.lastMin = minOf5Candle.low
        }
        this.min = minOf5Candle.low
      } else if (this.lastTrend === Trend.DOWN && this.status === Trend.UP) {
        let maxOf5Candle = array5CandleNear[2]
        let checkMax = false;
        for (let i = 1; i < compareCandle + 2; i++) {
          if ((this.lastMax / currentTick.close - 1) * 100 >= Config.REBOUND_FACTOR) {
            if (array5CandleNear[i].close > this.max) {
              maxOf5Candle = array5CandleNear[i]
            }
            console.log(`time đạt đỉnh: ${convertTime(maxOf5Candle.time)} - ${maxOf5Candle.high} - ${maxOf5Candle.high}`)
            console.log(`đỉnh cũ ${this.lastMax} - đáy cũ ${this.lastMin} - đỉnh hiện tại ${this.max} - đáy hiện tại ${this.min} - trend ${this.status} - last trend ${this.lastTrend}`)
            if (this.lastTrend === Trend.DOWN && this.status === Trend.UP) {
              this.lastTrend = Trend.UP
              this.status = Trend.DOWN
              checkMax = true
            }
          }
        }
        if (checkMax) {
          this.message(symbol)
          this.lastMax = maxOf5Candle.high
        }
        this.max = maxOf5Candle.high
      }
    } catch (e) {
      console.log(e)
    }
  }
}