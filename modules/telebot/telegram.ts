import { Config } from './../../constants';
import { Constants, periods } from '../../constants';
import { CandlestickData, CandlestickWebSocket, MiniTicker } from "../binance/binance";
import { TelegramBotHelper } from './helper';
import { TeleBot } from "./type"
import * as fs from "fs"

const TelegramBot = require("node-telegram-bot-api");

export const RegisterBot = async (tokens: TeleBot.BotType[]): Promise<any> => {
    console.log(tokens);
    tokens.forEach(async (TeleBot: TeleBot.BotType) => {
        const bot = new TelegramBot(TeleBot.token, { polling: true });
        await CandlestickWebSocket(bot, "BTCUSDT", periods.M15, TeleBot);
        // // Matches "/echo [whatever]"
        bot.onText(/\/signal/, (msg: any, match: any) => {
            // 'msg' is the received Message from Telegram
            // 'match' is the result of executing the regexp above on the text content
            // of the message
            const chatId = msg.chat.id;
            console.log("chatId", chatId);
            const rawdata = fs.readFileSync('./last-signal.json', 'utf8');
            const data = JSON.parse(rawdata)
            bot.sendMessage(chatId, data.text);
        });
    });
};
// Create a bot that uses 'polling' to fetch new updates
