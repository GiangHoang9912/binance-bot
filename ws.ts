import WebSocket from "ws";
import { Server } from "http";
import GeneralWsController from "./modules/webSockets/webSocket.ws";

import url from "url";

const setRoute = (
  pathname: string | null,
  path: string,
  wsHandler: WebSocket.Server,
  request: any,
  socket: any,
  head: any
): boolean => {
  if (pathname === path) {
    wsHandler.handleUpgrade(request, socket, head, function done(ws) {
      wsHandler.emit("connection", ws, request);
    });
    return true;
  }
  return false;
};

export function createWsServer(server: Server) {
  server.on("upgrade", function upgrade(request: any, socket, head) {
    const pathname = url.parse(request.url).pathname;
    const allRoute = [
      setRoute(
        pathname,
        "/ws/v1/general",
        GeneralWsController,
        request,
        socket,
        head
      ),
    ];
    if (allRoute.every((item) => !item)) {
      socket.destroy();
    }
  });

  return server;
}
