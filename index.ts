import {
  CandlestickData,
  CandlestickWebSocket,
  FutureChart,
  FuturePrice,
  LastPriceWebSocket,
  PlaceMutipleOrder,
} from "./modules/binance/binance";
import http from "http";
import { httpServer } from "./http";
import { getConfig } from "./libs/get_config";
import { connectRedis } from "./libs/cache_helper";
import { createWsServer } from "./ws";
import { RegisterBot } from "./modules/telebot/telegram";
import { Constants, periods } from "./constants";

async function main() {
  await connectRedis();
  const server = http.createServer(httpServer.callback());
  createWsServer(server);
  server.listen(getConfig("APP_PORT"));

  const tokens = [{
    token: Constants.TELEGRAM.TOKEN,
    chatId: Constants.TELEGRAM.CHAT_ID,
  }];

  //await FutureChart("BTCUSDT", periods.M15);
  const bot = await RegisterBot(tokens);
  await CandlestickWebSocket(bot, "BTCUSDT", periods.M15, {
    token: Constants.TELEGRAM.TOKEN,
    chatId: Constants.TELEGRAM.CHAT_ID,
  });

  console.log(
    `App "${getConfig("app.name") + " dev"}" run on ${getConfig("APP_PORT")}`
  );
}

void main();
