export const Constants = {
    API_URL: {
    },
    BINANCE: {
        APIKEY: "iyncGcE3ksbMy046jh6oBMBS2vciWTNBqm927GTtrPcRWkTYyAPc89GKoWj4u07W",
        APISECRET: "k2v4cIm5fFJWMsrm1K2xSSFspJC6ye1XcRDDJagfDz1XrDQPGSKSEydbgOpTQQAn",
    },
    TELEGRAM: {
        TOKEN: "5628665495:AAE5rASCw39bCmAxR2PAXPsZE0VtY1kRV08",
        CHAT_ID: "-1001544419277"
        // TOKEN: "5240579794:AAENnSpO_0I3yuesnsYtjPUqBNQagoBVMUY",
        // CHAT_ID: "1124702150"
    },
    PROXIES: [
        "192.142.225.68:45786:45785:Seloneblue368:D3b5FxC",
        "89.191.234.64:45786:45785:Seloneblue368:D3b5FxC",
        "141.98.235.225:45786:45785:Seloneblue368:D3b5FxC",
        "45.82.87.11:45786:45785:Seloneblue368:D3b5FxC",
        "194.9.179.3:45786:45785:Seloneblue368:D3b5FxC",
        "68.67.198.46:45786:45785:Seloneblue368:D3b5FxC",
        "79.141.161.122:45786:45785:Seloneblue368:D3b5FxC",
        "50.114.84.20:45786:45785:Seloneblue368:D3b5FxC",
        "103.230.69.246:45786:45785:Seloneblue368:D3b5FxC",
        "173.244.32.232:45786:45785:Seloneblue368:D3b5FxC",
        "181.214.107.118:45786:45785:Seloneblue368:D3b5FxC",
        "191.96.58.251:45786:45785:Seloneblue368:D3b5FxC",
        "192.161.184.24:45786:45785:Seloneblue368:D3b5FxC",
        "185.174.103.1:45786:45785:Seloneblue368:D3b5FxC",
        "185.33.85.142:45786:45785:Seloneblue368:D3b5FxC",
    ],
}

export enum periods {
    M1 = "1m",
    M3 = "3m",
    M5 = "5m",
    M15 = "15m",
    M30 = "30m",
    H1 = "1h",
    H2 = "2h",
    H4 = "4h",
    H6 = "6h",
    H8 = "8h",
    H12 = "12h",
    D1 = "1d",
    D3 = "3d",
    W1 = "1w",
    MM1 = "1M",
    MM3 = "3M",
    MM6 = "6M",
    Y1 = "1y",
    Y2 = "2y",
    Y5 = "5y",
}


export let Config = {
    TOP_BOTTON_RATIO: 2,
    TP: {
        TP1: 1,
        TP2: 1.44,
        TP3: 1.86,
        TP4: 2.4,
        TP5: 3,
        TP6: 3.8,
        TP7: 4.5,
        TP8: 5.4,
        TP9: 6.4,
        TP10: 7.1,
        TP11: 8,
    },
    SL: {
        SL_RATIO: 0.3,
        SL_TOP: 0,
        SL_BOTTON: 0,
    },
    ENTRY: {
        ENTRY1: 0.3,
        ENTRY2: 0.4,
        ENTRY3: 0.5,
    },
    MAX_RATIO: 3,
    MIN_RATIO: 3,
    REBOUND_FACTOR: 0.68,
    MAX: 0,
    MIN: 0,
}

export enum Trend {
    UP = "UP",
    DOWN = "DOWN",
    NONE = "NONE",
};