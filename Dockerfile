FROM node:17-alpine as development
WORKDIR /usr/src/app
COPY package.json .
COPY yarn.lock .
RUN yarn install --frozen-lockfile --ignore-scripts
COPY . .
RUN yarn prisma generate

FROM node:17-alpine as builder
WORKDIR /usr/src/app
COPY prisma .
COPY --from=development /usr/src/app/package.json .
COPY --from=development /usr/src/app/yarn.lock .
COPY --from=development /usr/src/app/node_modules ./node_modules
COPY . .
RUN yarn tsc
RUN yarn install --production=true --frozen-lockfile && yarn cache clean
# RUN yarn prisma generate

FROM node:17-alpine As production
WORKDIR /usr/src/app
ENV NODE_ENV production
ENV TZ="Asia/Ho_Chi_Minh"
COPY --from=builder /usr/src/app/ ./
CMD [ "node", "dist" ]
